# *WORK IN PROCESS*

# Yuki

Boilerplate for api in typescript. Very much work in process as missing `GraphQL` and any sort of logic.

All in `TypeScript` except `seeders` as these are used by `sequelize-cli`

### Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

* node v10.0.0
* yarn v1.6.0
* MySql v14.14
* Redis v4.0.9

### Development `.env`

```
NODE_ENV=development
PORT=3000
REDIS_HOST=127.0.0.1
REDIS_PORT=6379
REDIS_SECRET=SECRET
DATABASE=yuki
DATABASE_USERNAME=yuki
DATABASE_PASSWORD=yuki
DATABASE_HOST=127.0.0.1
DATABASE_PORT=3306
```
### Installing - Development

```
$ git clone ...
$ cd clone
$ touch .env
$ mysql -u root -p -e "CREATE DATABASE IF NOT EXISTS yuki;";
$ mysql -u root -p -e "CREATE USER 'yuki'@'localhost' IDENTIFIED BY 'yuki';"
$ mysql -u root -p -e "GRANT ALL ON yuki.* TO 'yuki'@'localhost' IDENTIFIED BY 'yuki';"
$ mysql -u root -p -e "FLUSH PRIVILEGES;"
$ yarn install
$ yarn start
```

*N.B. Havent add migrations yet!! So need to `yarn start` to create tables before `seeding`*

### Tests

*tbc*

### Coding style
All style rules are defined within `tslint.json`.

```
$ yarn lint
```
*NB* - Running `yarn start` will flag style errors.

```
// For Webstorm set up
Preferences > Langauge & Framworks > Typescript > TSLint > Enable
Preferences > Tools > File Watchers // Add prettier: one for .ts and another for .tsx
```

### Deployment

*tbc*

### Built With

* [Typescript](https://www.typescriptlang.org/)
* [Express](https://expressjs.com/)
* [GraphQL](https://graphql.org/)

### Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags).

### Authors

* **Alan Williams**
