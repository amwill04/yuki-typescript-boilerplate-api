const dotenv = require('dotenv').config();

module.exports = {
    [process.env.NODE_ENV]: {
        database: process.env.DATABASE,
        username: process.env.DATABASE_USERNAME,
        password: process.env.DATABASE_PASSWORD,
        host: process.env.DATABASE_HOST,
        port: process.env.DATABASE_PORT,
        dialect: 'mysql',
        migrationStorage: 'sequelize',
        seederStorage: 'sequelize',
    }
};