import bcrypt from 'bcrypt';
import { CompanyInstance, CompanyModel } from 'models/Company';
import Sequelize, { DefineAttributeColumnOptions } from 'sequelize';
import { SequelizeAttributes } from 'types/index';

export interface UserAttributes {
    readonly id?: number;
    email: DefineAttributeColumnOptions;
    firstName: DefineAttributeColumnOptions;
    lastName: DefineAttributeColumnOptions;
    password: DefineAttributeColumnOptions | string;
    readonly companyId?: number;
    readonly createdAt?: DefineAttributeColumnOptions;
    readonly updatedAt?: DefineAttributeColumnOptions;
    readonly company?: CompanyInstance;
    comparePassword?(password: string): boolean;
}

export type UserInstance = Sequelize.Instance<UserAttributes> & UserAttributes;

export interface UserModel extends Sequelize.Model<UserInstance, UserAttributes> {
    prototype?: {
        comparePassword?(password: string): boolean;
    };
}

// tslint:disable:object-literal-sort-keys
export default (sequelize: Sequelize.Sequelize): UserModel => {
    const attributes: SequelizeAttributes<UserAttributes> = {
        email: {
            allowNull: false,
            type: Sequelize.STRING(50),
            validate: {
                isEmail: true,
            },
        },
        firstName: {
            allowNull: false,
            type: Sequelize.STRING(20),
            validate: {
                isAlpha: true,
                notEmpty: true,
            },
        },
        lastName: {
            allowNull: false,
            type: Sequelize.STRING(20),
            validate: {
                isAlpha: true,
                notEmpty: true,
            },
        },
        password: {
            allowNull: false,
            type: Sequelize.STRING(255),
            set(value: string) {
                const hash: string = bcrypt.hashSync(value, 8);
                this.setDataValue('password', hash);
            },
        },
        createdAt: {
            allowNull: false,
            type: Sequelize.DATE,
            defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
        },
        updatedAt: {
            allowNull: false,
            type: Sequelize.DATE,
            defaultValue: sequelize.literal('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'),
        },
    };
    const User: UserModel = sequelize.define<UserInstance, UserAttributes>('user', attributes);
    User.associate = ({ Company }: { Company: CompanyModel }) =>
        User.belongsTo(Company, { hooks: true, onDelete: 'cascade' });
    User.prototype.comparePassword = function(password: string) {
        return bcrypt.compareSync(password, this.password);
    };
    return User;
};
