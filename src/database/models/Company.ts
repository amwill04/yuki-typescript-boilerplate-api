import Sequelize, { DefineAttributeColumnOptions } from 'sequelize';
import { SequelizeAttributes } from 'types/index';

interface CompanyAttributes {
    readonly id?: number;
    name: DefineAttributeColumnOptions;
    readonly createdAt?: DefineAttributeColumnOptions;
    readonly updatedAt?: DefineAttributeColumnOptions;
}

export type CompanyInstance = Sequelize.Instance<CompanyAttributes> & CompanyAttributes;

export type CompanyModel = Sequelize.Model<CompanyInstance, CompanyAttributes>;

// tslint:disable:object-literal-sort-keys
export default (sequelize: Sequelize.Sequelize): CompanyModel => {
    const attributes: SequelizeAttributes<CompanyAttributes> = {
        name: {
            type: Sequelize.STRING(50),
        },
        createdAt: {
            allowNull: false,
            type: Sequelize.DATE,
            defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
        },
        updatedAt: {
            allowNull: false,
            type: Sequelize.DATE,
            defaultValue: sequelize.literal('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'),
        },
    };
    return sequelize.define<CompanyInstance, CompanyAttributes>('companies', attributes, {
        freezeTableName: true,
    });
};
