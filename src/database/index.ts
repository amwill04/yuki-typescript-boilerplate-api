import Sequelize from 'sequelize';
import { database, isProd } from 'utils/env';

// import all models indevidually to maintain type inference
import Company from 'models/Company';
import User from 'models/User';

const sequelize = new Sequelize({
    database: database.database,
    dialect: 'mysql',
    host: database.host,
    logging: !isProd && console.log,
    operatorsAliases: false,
    password: database.password,
    port: parseInt(database.port, 10),
    timezone: 'Europe/London',
    username: database.username,
});

const Models = {
    Company: Company(sequelize),
    User: User(sequelize),
};

Object.values(Models).forEach((model: any) => {
    if (model.associate) {
        model.associate(Models);
    }
});

// tslint:disable:object-literal-sort-keys
export default {
    sequelize,
    Sequelize,
    ...Models,
};
