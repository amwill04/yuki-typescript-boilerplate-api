import passport from 'passport';
import Local from 'passport-local';

import Models from 'Models';
import { UserInstance } from 'models/User';

const { User } = Models;

const LocalStrategy = Local.Strategy;

passport.serializeUser<UserInstance, number>((user, done) => {
    done(undefined, user.id);
});

passport.deserializeUser<UserInstance, number>((id, done) => {
    User.findById(id).then((user: UserInstance) => done(null, user));
});

passport.use(
    new LocalStrategy({ usernameField: 'email' }, (email, password, done) => {
        User.findOne({ where: { email: email.toLowerCase() } })
            .then((user: UserInstance) => {
                if (!user) {
                    return done(undefined, false, { message: 'Email and/or password is incorrect' });
                }
                if (!user.comparePassword(password)) {
                    return done(undefined, false, { message: 'Email and/or password is incorrect' });
                }
                const { email: userEmail, firstName, id, lastName } = user;
                done(undefined, { email: userEmail, firstName, id, lastName });
            })
            .catch((error: any) => done(error));
    })
);

export default passport;
