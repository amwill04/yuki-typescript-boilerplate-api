import http from 'http';
import { port } from 'utils/env';
import app from './app';
import Models from './database/index';

const server = http.createServer(app);

Models.sequelize.sync().then(() => server.listen(port));
