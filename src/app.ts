import connectRedis from 'connect-redis';
import express, { Application } from 'express';
import session from 'express-session';
import { isProd, REDIS_SECRET, redisOptions } from 'utils/env';
import passport from './config/passport';
import Routes from './http/routes';
const app: Application = express();

// app settings
app.disable('x-powered-by');

// Intiate Redis Store for express-session
const RedisStore = connectRedis(session);
app.use(
    session({
        cookie: {
            // https://www.owasp.org/index.php/Session_Management_Cheat_Sheet
            // non-persistent cookie - no max-age and/or expires
            // TODO change path to api
            // secure force HTTPS only for production
            expires: null,
            httpOnly: true,
            maxAge: null,
            path: '/',
            sameSite: 'strict',
            secure: isProd,
        },
        resave: false,
        saveUninitialized: false,
        secret: REDIS_SECRET,
        store: new RedisStore(redisOptions),
    })
);

app.use(passport.initialize());
app.use(passport.session());

// Load routes
Routes(app);

export default app;
