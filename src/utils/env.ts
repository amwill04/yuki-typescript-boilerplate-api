import { RedisStoreOptions } from 'connect-redis';
import dotenv from 'dotenv';
import fs from 'fs';

if (!fs.existsSync('.env')) {
    throw Error('Missing ".env"');
}

dotenv.config({ path: '.env' });

// Standard varaibles
export const isProd: boolean = process.env.NODE_ENV === 'production';

export default process.env.NODE_ENV;

// Redis && Session options
export const port = process.env.PORT;

export const redisOptions: RedisStoreOptions = {
    host: process.env.REDIS_HOST,
    logErrors: isProd ? console.error : console.log,
    port: parseInt(process.env.REDIS_PORT, 10),
    ttl: 43200, // 12 hour MAX session regardless of activity https://www.owasp.org/index.php/Session_Management_Cheat_Sheet See - Absolute Timeout.
};

export const REDIS_SECRET = process.env.REDIS_SECRET;

////////////////////////////////////////////////////

// Database
export const database = {
    database: process.env.DATABASE,
    host: process.env.DATABASE_HOST,
    password: process.env.DATABASE_PASSWORD,
    port: process.env.DATABASE_PORT,
    username: process.env.DATABASE_USERNAME,
};
