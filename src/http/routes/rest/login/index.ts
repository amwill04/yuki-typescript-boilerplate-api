import bodyParser from 'body-parser';
import express from 'express';
import loginConstructor from './login.constructor';

const router = express.Router();
const urlParser = bodyParser.urlencoded({
    extended: false,
    parameterLimit: 2,
    type: 'application/x-www-form-urlencoded',
});

export default router.post('/login', urlParser, loginConstructor);
