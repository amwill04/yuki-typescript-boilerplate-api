import { NextFunction, Request, Response } from 'express';
import { UserModel } from 'models/User';
import passport from 'passport';
import { IVerifyOptions } from 'passport-local';

export default (req: Request, res: Response, next: NextFunction): void => {
    passport.authenticate('local', (err: Error, user: UserModel, info: IVerifyOptions) => {
        if (err) {
            return next(err);
        }
        if (!user) {
            return res.json({ message: info.message });
        }
        req.logIn(user, (logInErr: Error) => {
            if (logInErr) {
                next(logInErr);
            }
            return res.json(user);
        });
    })(req, res, next);
};
