import { Application } from 'express';
import login from './rest/login';

export default (app: Application) => {
    app.use('/', [login]);
};
