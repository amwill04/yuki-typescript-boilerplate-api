'use strict';
const bcrypt = require('bcrypt');


module.exports = {
    up: queryInterface => {
        return queryInterface.bulkInsert('users', [{
            firstName: 'Joe',
            lastName: 'Joe',
            email: 'email@email.com',
            companyId: 1,
            password: bcrypt.hashSync('Welcome123', 8),
        }], {});
    },

    down: queryInterface => {
        return queryInterface.bulkDelete('users', null, {});
    }
};
