'use strict';

module.exports = {
    up: queryInterface => {
        return queryInterface.bulkInsert('companies', [{
            name: 'Dummy Company',
        }], {});
    },

    down: queryInterface => {
        return queryInterface.bulkDelete('companies', null, {});
    }
};
